package com.example.cocktailapi.util

import com.example.cocktailapi.model.models.Cocktails

sealed class Resource<out T> {
    object Idle: Resource<Nothing>()
    data class Success<T>(val data: T): Resource<T>()
    object Loading: Resource<Nothing>()
    data class Error(val error: Throwable) : Resource<Nothing>()
}
