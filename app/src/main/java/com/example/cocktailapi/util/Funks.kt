package com.example.cocktailapi.util

import android.app.Activity
import android.content.Context
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import java.io.IOException

fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}

fun Fragment.loggerToast(msg: String) {
    Log.d("LOGD", msg)
    Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
}

fun getJsonDataFromAsset(context: Context, fileName: String): String? {
    val jsonString: String
    try {
        jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
    } catch (ioException: IOException) {
        ioException.printStackTrace()
        return null
    }
    return jsonString
}
