package com.example.cocktailapi.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailapi.model.CocktailRepo
import com.example.cocktailapi.model.models.Cocktail
import com.example.cocktailapi.model.models.Cocktails
import com.example.cocktailapi.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CocktailViewmodel : ViewModel(){

    private val repo = CocktailRepo

    private val _liveData : MutableLiveData<Resource<Cocktails?>> = MutableLiveData(Resource.Idle)
    val liveData : LiveData<Resource<Cocktails?>> get() = _liveData


    fun listFiveCocktails() =   viewModelScope.launch(Dispatchers.Main) {
            _liveData.value = repo.getRandomCocktail()
        }
}