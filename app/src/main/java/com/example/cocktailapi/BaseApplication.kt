package com.example.cocktailapi

import android.app.Application

class BaseApplication  : Application() {

    override fun onCreate() {
        super.onCreate()
        FlipperProvider.init(this)
    }
}