package com.example.cocktailapi.model

import android.util.Log
import com.example.cocktailapi.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

object CocktailRepo {

    private val barService by lazy { CocktailApi.drinkRetrofit }

    suspend fun getRandomCocktail() = withContext(Dispatchers.IO) {
        return@withContext try{
            val response = barService.getRandomCocktail()
            if(response.isSuccessful){
                Resource.Success(response.body())
            }
            else{
                Log.d("LOGD", "API Error")
                Resource.Error(Throwable())
            }
        }
        catch(e: Exception){
            Log.d("LOGD", "Full Repo Fail: ${e.localizedMessage}")

            Resource.Error(Throwable())
        }
    }
}