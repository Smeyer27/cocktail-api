package com.example.cocktailapi.model

import com.example.cocktailapi.FlipperProvider
import com.example.cocktailapi.model.models.Cocktails
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface CocktailApi  {

    @GET("random.php")

    suspend fun getRandomCocktail() : Response<Cocktails>

    companion object {

        private val okhttp by lazy {
            OkHttpClient.Builder().addNetworkInterceptor(FlipperProvider.getFlipperOkhttpInterceptor()).build()
        }

        val drinkRetrofit: CocktailApi by lazy {
            Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://www.thecocktaildb.com/api/json/v1/1/")
                .client(okhttp)
                .build()
                .create(CocktailApi::class.java)
        }
    }
}