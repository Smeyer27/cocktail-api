package com.example.cocktailapi.model.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Cocktails(
    val drinks : List<Cocktail>
): Parcelable


