package com.example.cocktailapi.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailapi.databinding.CocktailCardBinding
import com.example.cocktailapi.model.models.Cocktail
import com.example.cocktailapi.util.loadImage

class CocktailAdapter (
    private val itemClicked: (title : Cocktail) -> Unit) : RecyclerView.Adapter<CocktailAdapter.CocktailViewHolder>() {

    private lateinit var cocktailList : List<Cocktail>

    class CocktailViewHolder (private val binding : CocktailCardBinding): RecyclerView.ViewHolder(binding.root) {
        fun applyToCard (cocktailInfo : Cocktail) = with(binding) {
            cocktailCardName.text = cocktailInfo.strDrink
            cocktailCardImg.loadImage(cocktailInfo.strDrinkThumb)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CocktailViewHolder {
        val binding = CocktailCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CocktailViewHolder(binding).apply {
            binding.root.setOnClickListener {
                itemClicked(cocktailList[adapterPosition])
            }
        }
    }

    override fun onBindViewHolder(holder: CocktailViewHolder, position: Int) {
        val singleItem = cocktailList[position]
        holder.applyToCard(singleItem)
    }

    override fun getItemCount(): Int {
        return cocktailList.size
    }
     fun addFromCardToList(items: List<Cocktail>) {
         cocktailList = items
     }

}