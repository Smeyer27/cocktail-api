package com.example.cocktailapi.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cocktailapi.databinding.FragmentCocktailListBinding
import com.example.cocktailapi.model.models.Cocktail
import com.example.cocktailapi.util.Resource
import com.example.cocktailapi.util.getJsonDataFromAsset
import com.example.cocktailapi.util.loggerToast
import com.example.cocktailapi.viewmodel.CocktailViewmodel
import com.example.cocktailapi.views.adapters.CocktailAdapter
import com.google.gson.Gson

class CocktailListFragment : Fragment() {

    private var _binding : FragmentCocktailListBinding? = null
    private val binding: FragmentCocktailListBinding get() = _binding!!

    private val viewmodel by activityViewModels<CocktailViewmodel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCocktailListBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = getJsonDataFromAsset(requireContext(), "randomCocktail.json")
        val actualData = Gson().fromJson(data, Cocktail::class.java)
        initViews()
        loggerToast("onViewCreated")
    }

    private fun initViews() = with(binding) {

        repeat(5) {
            viewmodel.listFiveCocktails()
        }

        val drinkList = mutableListOf<Cocktail>()

        viewmodel.liveData.observe(viewLifecycleOwner) {
            viewState ->
            loggerToast("onViewCreated: ${viewState}")

            when(viewState) {
                is Resource.Idle -> {
                    loggerToast("Resource Idle")
                    progCirc.visibility = View.GONE

                }
                is Resource.Error -> {
                    loggerToast("List/ LiveData Load Error")
                    progCirc.visibility = View.GONE

                }
                is Resource.Loading -> {
                    loggerToast("Resource Loading")
                    progCirc.visibility = View.VISIBLE
                }
                is Resource.Success -> {
                    loggerToast("Cocktail Passed through successfully")
                    progCirc.visibility = View.GONE
                    cocktailRecycler.layoutManager = LinearLayoutManager(requireContext())
                    drinkList.add(viewState.data?.drinks?.get(0) ?: Cocktail(
                        "1", "No Ccktails Here", "", ""))
                }
                else -> {}
            }
            if (drinkList.size.equals(5)) {
                loggerToast("List Populated")
                binding.cocktailRecycler.adapter = CocktailAdapter(::itemClicked).apply {
                    addFromCardToList(drinkList)
                }
            }
        }
        loggerToast("List View Initialized")
    }
    private fun itemClicked ( cocktail : Cocktail){
        val action = CocktailListFragmentDirections.actionCocktailListFragmentToCocktailDetailFragment(cocktail = cocktail)
        findNavController().navigate(action)
        loggerToast("Clicked through to Detail")
    }
}