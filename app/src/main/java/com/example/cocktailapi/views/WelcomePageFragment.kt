package com.example.cocktailapi.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.cocktailapi.databinding.FragmentWelcomePageBinding
import com.example.cocktailapi.util.loggerToast

class WelcomePageFragment : Fragment() {

    private var _binding : FragmentWelcomePageBinding? = null
    private val binding: FragmentWelcomePageBinding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentWelcomePageBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) = with(binding){
        super.onViewCreated(view, savedInstanceState)
        welcomeBtn.setOnClickListener{
            findNavController().navigate(WelcomePageFragmentDirections.actionWelcomePageFragmentToCocktailListFragment())
        }
        loggerToast("Welcome Page Init")
    }


}