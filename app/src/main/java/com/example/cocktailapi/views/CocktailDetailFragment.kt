package com.example.cocktailapi.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.example.cocktailapi.databinding.FragmentCocktailDetailBinding
import com.example.cocktailapi.util.loadImage

class CocktailDetailFragment : Fragment() {

    private var _binding : FragmentCocktailDetailBinding? = null
    private val binding : FragmentCocktailDetailBinding get() = _binding!!
    private val args by navArgs<CocktailDetailFragmentArgs>()

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    )= FragmentCocktailDetailBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding){
        cocktailName.text = args.cocktail.strDrink
        cocktailImg.loadImage(args.cocktail.strDrinkThumb)
        cocktailDetail.text = args.cocktail.strInstructions
    }
}